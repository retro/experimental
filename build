#! /bin/bash
#! --------------------------------------------------------------
#! Retro Development Kit
#!
#! This is the build script for RDEV. It is currently being
#! updated to be a bit more modular and easier to extend in the
#! future.
#! --------------------------------------------------------------

SDL="no"
THIS=`pwd`


#! --------------------------------------------------------------
#! Read input from the user
#! --------------------------------------------------------------
input()
{
  read response
  echo $response
}


#! --------------------------------------------------------------
#! Display menu of build options
#! --------------------------------------------------------------
function options
{
  clear
  echo Retro Development Kit                    http://retroforth.org
  echo --------------------------------------------------------------
  echo Please select the target you wish to build from the list
  echo below.
  echo
  echo 1. Console Build 2 \(faster VM\)
  echo 2. Retro image for Ngaro JS
  echo 3. Java Web Build
  echo 4. J2ME
  echo
  echo Choice:

    option=`input`;
    case $option in
      [1])
        toka
        image
        ngaro_mat_tty
        cont=0
      ;;
      [2])
        toka
        image
        image_js
        cont=0
      ;;
      [3])
        toka
        image
        image_java
        cont=0
      ;;
      [4])
        toka
        image
        image_j2me
        cont=0
      ;;
    esac
}


#! --------------------------------------------------------------
#! Build Toka
#! --------------------------------------------------------------
function toka
{
  cd $THIS/toka
  gcc *.c -o toka
  cd $THIS
  mv toka/toka bin
}



#! --------------------------------------------------------------
#! Build Ngaro (C, with SDL)
#! --------------------------------------------------------------
function ngaro_crc_sdl
{
  cd $THIS/ngaro/c-crc/sdl
  gcc -Wall -O3 -fomit-frame-pointer disassemble.c endian.c loader.c ngaro.c devices.c vm.c -DUSE_SDL -Wall `sdl-config --cflags --libs` -o ngaro
  cd $THIS
  mv ngaro/c-crc/sdl/ngaro bin
}



#! --------------------------------------------------------------
#! Build Ngaro (C, text only)
#! --------------------------------------------------------------
function ngaro_crc_tty
{
  cd $THIS/ngaro/c-crc/console
  gcc -Wall -O3 -fomit-frame-pointer disassemble.c endian.c loader.c ngaro.c vm.c -o ngaro
  cd $THIS
  mv ngaro/c-crc/console/ngaro bin
}

function ngaro_mat_tty
{
  cd $THIS/ngaro/c-mat
  gcc -Wall -O3 -fomit-frame-pointer endian.c loader.c ngaro.c tty_devices.c vm.c -o ngaro
  cd $THIS
  mv ngaro/c-mat/ngaro bin
}



#! --------------------------------------------------------------
#! Build the RETRO image.
#! --------------------------------------------------------------
function image
{
  cd $THIS/retro
  ln -s $THIS/bin/bootstrap.toka
  $THIS/bin/toka retro.toka
  rm bootstrap.toka
  cd $THIS
  mv retro/forth.image bin
  mv retro/forth.image.map bin

  gcc _build/fix-image.c -o bin/fix-image
  cd $THIS/bin
  ./fix-image forth.image
}


function image_js
{
  cd $THIS/bin
  $THIS/bin/toka $THIS/_build/image2js.toka forth.image >image.js
}


function image_java
{
  cd $THIS/bin
  $THIS/bin/toka $THIS/_build/image2java.toka forth.image >$THIS/ngaro/java/image.java
  cd $THIS/ngaro/java
  cat Ngaro.top image.java Ngaro.bottom >NgaroVM.java
  javac NgaroVM.java
  rm image.java
  mv NgaroVM.class $THIS/bin
  rm NgaroVM.java
}


function image_j2me
{
  cd $THIS/bin
  $THIS/bin/toka $THIS/_build/image2j2me.toka forth.image >$THIS/ngaro/j2me/src/Retro/Img.java.middle
  cd $THIS/ngaro/j2me/src/Retro
  cat Img.java.top Img.java.middle Img.java.bottom >Img.java
  cd $THIS/ngaro/j2me
  ant
  rm src/Retro/Img.java.middle src/Retro/Img.java
  cp bin/* $THIS/bin
  rm bin/*.jar
}


options
