/*************************************************************
 * test.c
 *
 * This is a dummy application that does literally nothing. It
 * is used by the build scripts as an aid in determining the
 * proper linker flags via a trick I picked up from Helmar.
 *
 * This file is public domain.
 *************************************************************/
int main()
{
}
