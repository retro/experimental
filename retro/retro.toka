#! ------------------------------------------------------------
#!    __   ___ ____   __    __    
#!   /__) (_    /    /__)  /  )   
#!  / (   /__  (    / (   (__/    
#! ------------------------------------------------------------ )

reset

#! ============================================================
#! Configuration of memory layout
#! ============================================================

30720   is-data SCRATCH-START
31744   is-data TIB
32768   is-data HEAP-START
3500000 is-data STRING-START
5000000 is-data MAX-IMAGE


#! ============================================================
#! ASSEMBLER / CROSS-COMPILER
#! ============================================================

[ ." MODULE: " wsparse type cr ] is MODULE:
[ lnparse drop ] is AUTHOR:
[ lnparse drop ] is NOTES:

value| heap origin fid log lastop |

[ SPACE string.appendChar
  >r log r> string.getLength file.write drop ] is $log
[ log " \n" string.getLength file.write drop ] is \n

[ heap ! heap cell+ to heap ] is ,
[ [ dup to lastop , ] +action ] is vm:

 0 vm: nop,          1 vm: lit,          2 vm: dup,          
 3 vm: drop,         4 vm: swap,         5 vm: push,
 6 vm: pop,          7 vm: call,         8 vm: jump,
 9 vm: ;            10 vm: >jump,       11 vm: <jump,
12 vm: !jump,       13 vm: =jump,       14 vm: @,
15 vm: !,           16 vm: +,           17 vm: -,
18 vm: *,           19 vm: /mod,        20 vm: and,
21 vm: or,          22 vm: xor,         23 vm: <<,
24 vm: >>,          25 vm: 0;           26 vm: 1+,
27 vm: 1-,          28 vm: in,          29 vm: out,
30 vm: wait,        98 vm: halt,

#! Additional opcodes for Mat's extended Ngaro VM
31 vm: pick,        32 vm: back,        33 vm: rot,
34 vm: sinst,       35 vm: xop,         36 vm: exe,
37 vm: li_iac,      38 vm: li_iop,      39 vm: pick_iac,
40 vm: pick_iop,    41 vm: radd,        42 vm: rsub,
43 vm: rmul,        44 vm: rand,        45 vm: ror,
46 vm: rxor,        47 vm: lcall,       48 vm: lreturn,


[ MAX-IMAGE cells malloc keep to origin origin to heap 
  wsparse dup "W" file.open to fid
  " .map" string.append "W" file.open to log
  jump, 0 ,
] is begin
[ fid origin heap origin - file.write fid file.close
  heap origin - cell-size / . ." cells written. Goodbye!\n\n"
  log file.close bye 
] is end

[ heap origin - cell-size / ] is here
[ here origin cell+ ! ] is main:
[ here is-data 
  last @ :xt >string TAB string.appendChar $log
  " LABEL:" $log last @ :name $log \n
] is label:
[ lit, , ] is #
[ string.getLength 0 swap [ dup c@ , char+ ] countedLoop 
  drop 0 , ] is $,

[ here cells origin + 0 , ] is conditional
[ !jump, conditional ] is =if
[ >jump, conditional ] is <if
[ <jump, conditional ] is >if
[ =jump, conditional ] is !if
[ here swap ! nop, ] is then

[ here ] is repeat
[ jump, , ] is again

{
  variable getxt
  [ getxt on ]
  [ cell-size malloc here over ! nop, nop,
    [ getxt @ [ @ ] [ call, @ , ] ifTrueFalse 
      getxt off 
    ] +action 
    last @ :xt cell+ @ @ >string TAB string.appendChar $log
    " WORD:" $log last @ :name $log \n
  ]
} is :
  is '
[ label: , ] is variable:
[ 0 variable: ] is variable
[ 0 [ 0 , ] countedLoop ] is allot

value| link #entries |
[ #entries 1 + to #entries ] is e+1
[ e+1 here link , to link 0 , , wsparse $, ] is word:
[ e+1 here link , to link 1 , , wsparse $, ] is macro:
[ e+1 here link , to link 2 , , wsparse $, ] is data:

[ link r> ! ] is patch-dictionary
[ here cells origin + >r ] is mark-dictionary

value| init-link |
[ here cells origin + >r ] is mark-init-chain
[ init-link r> ! ] is patch-init-chain
[ here init-link , to init-link , ] is +init

value| TCE |
[ TCE 1 + to TCE ] is +TCE
[ lastop 8 <>
  [ lastop 7 =
    [ 8 here 2 - cells origin + ! +TCE ]
    [ ; ]
    ifTrueFalse
  ] ifTrue
] is ;
[ depth 0 <> [ ." BUGS DETECTED!\n" ] ifTrue
  #entries . ." entries\n" 
  TCE . ." tail calls eliminated\n" 
] is cross-summary



#! ============================================================
#! RETRO
#! ============================================================
begin forth.image

label: copytag   " RETRO 10" $,
label: nomatch   " ? not found" $,
label: okmsg     " ok " $,

mark-dictionary  variable last
mark-init-chain  variable init-chain

variable #mem

variable which
: d->class 1+, ;
: d->xt    1+, 1+, ;
: d->name  1+, 1+, 1+, ;

: dup      dup, ;       : 1+       1+, ;
: 1-       1-, ;        : swap     swap, ;
: drop     drop, ;      : and      and, ;
: or       or, ;        : xor      xor, ;
: @        @, ;         : !        !, ;
: +        +, ;         : -        -, ;
: *        *, ;         : /mod     /mod, ;
: <<       <<, ;        : >>       >>, ;
: out      out, ;       : in       in, ;
: wait     0 # 0 # out, wait, ;

: nip      swap, drop, ;
: over     push, dup, pop, swap, ;
: 2drop    drop, drop, ;
: not      -1 # xor, ;
: rot      push, swap, pop, swap, ;
: -rot     swap, push, swap, pop, ;
: tuck     dup, -rot ;
: 2dup     over over ;
: on       -1 # swap, !, ;
: off       0 # swap, !, ;
: /        /mod, nip ;
: mod      /mod, drop, ;
: neg      -1 # *, ;
: execute   1-, push, ;

: @+ dup, 1+, swap, @, ;
: !+ dup, 1+, push, !, pop, ;
: +! dup, push, @, +, pop, !, ;
: -! dup, push, @, swap, -, pop, !, ;

variable tx   ( text x coordinate  )
variable ty   ( text y coordinate  )
variable fb   ( framebuffer addr   )
variable fw   ( framebuffer width  )
variable fh   ( framebuffer height )
-1 variable: update

: redraw update # @, 0; drop, 0 # 3 # out, ;

: fb:cr 0 # tx # !, ty # @, 16 # +, ty # !, ;
: move tx # @, 16 # +, dup, tx # !, fw # @, >if fb:cr then ;

: fb:emit
  dup, 10 # =if fb:cr drop, ; then 
  dup, 13 # =if fb:cr drop, ; then 
  push, tx # @, ty # @, pop, 
  1 # 2 # out, wait move
  redraw
;

: fb:home 0 # tx # !, 0 # ty # !, ;

: fb:size fw # @, fh # @, *, ;
: (clear) repeat 0; dup, fb # @, +, 0 # swap, !, 1-, again ;
: fb:clear fb:home fb:size (clear) redraw ;

: tty:emit 1 # 2 # out, wait redraw ;
: tty:cr   10 # tty:emit ;
: tty:home 27 # tty:emit
  char: [ # tty:emit char: 1 # tty:emit
  char: ; # tty:emit char: 1 # tty:emit
  char: H # tty:emit ;
: tty:clear 27 # tty:emit
  char: [ # tty:emit char: 2 # tty:emit 
  char: J # tty:emit tty:home ;

: emit  fb # @, 0 # !if fb:emit  ; then tty:emit ;
: cr    fb # @, 0 # !if fb:cr    ; then tty:cr ;
: clear fb # @, 0 # !if fb:clear ; then tty:clear ;
: home  fb # @, 0 # !if fb:home  ; then tty:home ;

: (type) repeat @+ 0; emit again ;
: type update # off (type) drop, update # on redraw ;

: save 1 # 4 # out, wait ;
: bye jump, MAX-IMAGE , ; 

: words 
  last # @, repeat dup, d->name type 32 # emit @, 0; again ;

variable >in              ( Offset into the TIB )
variable break-char       ( Holds the delimiter )

: (remap-keys) ;

: key   ( -x )
  repeat
    1 # 1 # out,
    wait 1 # in,
    dup, 0 # !if (remap-keys) ; then drop,
  again
;

: >tib  ( x- )  TIB # >in # @, +, !, ;
: ++    ( - )   1 # >in # +! ;

: (eat-leading)   ( - )
  repeat key dup, emit dup, 
         break-char # @, !if >tib ++ ; then drop, again ;

: (accept)        ( -x )
  repeat key dup, emit dup, 
         break-char # @, =if ; then >tib ++ again ;

: accept          ( x- )
  break-char # !, 0 # >in # !, (eat-leading) (accept) drop, 
  0 # >tib ;

variable compiler
HEAP-START variable: heap

: compiling?  ( - )    compiler # @, 0 # =if pop, drop, ; then ;
: t-here      ( -a )   heap # @, ;
: t-,         ( n- )   t-here !, t-here 1+, heap # !, ;
: t-]         ( - )    -1 # compiler # !, ;
: t-[         ( - )    0 # compiler # !, ;
: t-;;        ( - )    compiling? 9 # t-, ;
: t-;         ( - )    compiling? t-;; t-[ ;
: ($,)        ( a-a )  repeat dup, @, 0; t-, 1+, again ;
: $           ( a- )   ($,) drop, 0 # t-, ;
: create      ( "- )   t-here              ( Entry Start )
                       last # @, t-,       ( Link to previous )
                       last # !,           ( Set as newest )
                       2 # t-,             ( Class = .data )
                       t-here 0 # t-,      ( XT, leave pointer )
                       32 # accept TIB # $ ( Name )
                       t-here swap, !, ;   ( Patch XT to HERE )
: (:)         ( - )    last # @, d->class !, t-] 0 # t-, 0 # t-, ;
: t-:         ( "- )   create 0 # (:) ;
: t-macro:    ( "- )   create 1 # (:) ;
: t-(         ( "- )   char: ) # accept ;
: t-push      ( - )    compiling? 5 # t-, ;
: t-pop       ( - )    compiling? 6 # t-, ;
: compile     ( a- )   7 # t-, t-, ;
: literal,    ( n- )   1 # t-, t-, ;
: t-for       ( - )    t-here 5 # t-, ;
: t-next      ( - )    6 # t-, 27 # t-, 25 # t-, 8 # t-, t-, ;

variable $flag
: n=n       ( xy- )         !if 0 # $flag # !, then ;
: get-set   ( ab-xy )       @, swap, @, ;
: next-set  ( ab-a+1 b+1 )  1+, swap, 1+, swap, ;

: (skim)
  repeat
    2dup @, swap, @, +, 0; drop,
    2dup get-set n=n next-set
    $flag # @, 0; drop,
  again
;

: compare   ( $1 $2 -- flag )
  -1 # $flag # !,
  (skim) 2drop $flag # @, ;

: (strlen)
  repeat dup, @, 0; drop, next-set again ;

: getLength ( $1 - n )
  0 # swap, (strlen) drop, ;

STRING-START variable: STRINGS
variable SAFE
variable LATEST

: (reset-$)  SCRATCH-START # SAFE # !, ;
: (next)     1 # SAFE # +! ;
: (copy)     repeat @+ 0; SAFE # @, !, (next) again ;

: tempString (reset-$) (copy) drop, 0 # SAFE # @, !, SCRATCH-START # ;
: keepString
  STRINGS # @, LATEST # !,
  STRINGS # @, SAFE # !, (copy) drop, 0 # SAFE # @, !, 
  SAFE # @, 1+, STRINGS # !,
  LATEST # @,
;

: t-" char: " # accept TIB # tempString ;
: t-s" compiling? 1 # t-, t-" keepString t-, ;

variable #value
variable #flag
variable num
variable #ok
variable negate?

: isdigit?
  dup, 47 # >if dup, 58 # <if drop, -1 # ; then then drop, 0 # ;

: char>digit 48 # -, ;

: digit>char 48 # +, ;

: isNegative?
  ( a-a+1 )
  dup, @, char: - # =if -1 # negate? # !, 1+, ; then 
  1 # negate? # !, ;

: (convert)
  repeat dup, @, 0; char>digit #value # @, 10 # *, +, 
         #value # !, 1+, again ;

: >number
  isNegative? 0 # #value # !, (convert) drop, 
  #value # @, negate? # @, *, ;

: (isnumber)
  repeat
    dup, @, 0; isdigit? #flag # @, and, #flag # !, 1+,
  again ;

: isnumber?
  isNegative? -1 # #flag # !, (isnumber) drop, 
  #flag # @, ;

: number>digits 
  ( x-... )
  1 # #flag # +!
  #value # @, 10 # /mod, 
  dup, 0 # !if #value # !, jump, ' number>digits , then 
  drop, ;

: digits>screen
  ( ...- )
  repeat #flag # @, 0; drop, digit>char emit 
         #flag # @, 1-, #flag # !, again ;

: .
  dup, 0 # <if dup, 0 # !if char: - # emit neg then then
  0 # #flag # !, #value # !, 
  number>digits digits>screen 32 # emit ;


variable found

: (search)
  repeat
    dup, d->name TIB # compare
    -1 # =if which # !, found # on ; then
    @, 0;
  again
;

: search
  found # off last # @, (search) ;

: t-=if    compiling? 12 # t-, t-here 0 # t-, ;
: t->if    compiling? 11 # t-, t-here 0 # t-, ;
: t-<if    compiling? 10 # t-, t-here 0 # t-, ;
: t-!if    compiling? 13 # t-, t-here 0 # t-, ;
: t-then   compiling? t-here swap, !, 0 # t-, ;
: t-repeat compiling? t-here ;
: t-again  compiling? 8 # t-, t-, ;
: t-0;     compiling? 25 # t-, ;
: :devector dup, 0 # swap, !, 1+, 0 # swap, !, ;
: :is       dup, 8 # swap, !, 1+, !, ;
: t-'      32 # accept search
           found # @, -1 # =if which # @, d->xt @, ; then 0 # ;
: t-[']    compiling? 1 # t-, t-' t-, ;
: devector t-' :devector ; dup, 0 # swap, !, 1+, 0 # swap, !, ;
: is       t-' :is ; dup, 8 # swap, !, 1+, !, ;

variable cold
: run-once
  -1 # 5 # out, wait 5 # in, #mem # !,  ( Memory Size )
  -2 # 5 # out, wait 5 # in, fb # !,    ( Framebuffer Addr )
  -3 # 5 # out, wait 5 # in, fw # !,    ( Framebuffer Width )
  -4 # 5 # out, wait 5 # in, fh # !,    ( Framebuffer Height )
  cold # @, 0 # =if copytag # type cr -1 # cold # !, then ;

: .word  compiler # @, -1 # =if 7 # t-, t-, ; then execute ;
: .macro execute ;
: .data  compiler # @, -1 # =if 1 # t-, t-, then ;

: with-class  ( ac- )
  dup, 0 # =if drop, .word  ; then
  dup, 1 # =if drop, .macro ; then
  dup, 2 # =if drop, .data  ; then
  execute
;

: notfound cr nomatch # type cr ;

: find-word
  found # @, -1 # =if which # @, dup, d->xt @, swap, d->class @, with-class then ;
: find-number
  found # @, 0 # =if TIB # isnumber? 
  -1 # =if TIB # >number 2 # with-class ; then notfound then ;

: ok compiler # @, 0 # =if cr okmsg # type then ;

: listen  ( - )
  repeat ok 32 # accept search find-word find-number again ;

: initialize
  init-chain # @, repeat dup, 1+, @, execute @, 0; again ;


#! ----------------------------------------------------------
#! Things to run on startup
#! ----------------------------------------------------------
' run-once +init


#! ----------------------------------------------------------
#! Map words into the dictionary
#! ----------------------------------------------------------

' t-here       word: here          ' t-,          word: ,
' t-]          word: ]             ' create       word: create
' t-:          word: :             ' t-macro:     word: macro:
' cr           word: cr            ' emit         word: emit
' home         word: home          ' type         word: type
' clear        word: clear         ' save         word: save
' words        word: words         ' key          word: key
' accept       word: accept        ' dup          word: dup
' 1+           word: 1+            ' 1-           word: 1-
' swap         word: swap          ' drop         word: drop
' and          word: and           ' or           word: or
' xor          word: xor           ' @            word: @
' !            word: !             ' +            word: +
' -            word: -             ' *            word: *
' /mod         word: /mod          ' <<           word: <<
' >>           word: >>            ' nip          word: nip
' over         word: over          ' 2drop        word: 2drop
' not          word: not           ' rot          word: rot
' -rot         word: -rot          ' tuck         word: tuck
' 2dup         word: 2dup          ' on           word: on
' off          word: off           ' /            word: /
' mod          word: mod           ' neg          word: neg
' execute      word: execute       ' .            word: .
' t-"          word: "             ' compare      word: compare
' in           word: in            ' out          word: out
' wait         word: wait          ' t-'          word: '
' @+           word: @+            ' !+           word: !+
' +!           word: +!            ' -!           word: -!
' :is          word: :is           ' :devector    word: :devector
' is           word: is            ' devector     word: devector
' compile      word: compile       ' literal,     word: literal,
' tempString   word: tempString    ' redraw       word: redraw
' keepString   word: keepString    ' getLength    word: getLength
' bye          word: bye           ' (remap-keys) word: (remap-keys)
' with-class   word: with-class    ' .word        word: .word
' .macro       word: .macro        ' .data        word: .data
' d->class     word: d->class      ' d->xt        word: d->xt
' d->name      word: d->name

( Macros )
' t-s"         macro: s"           ' t-[          macro: [
' t-;          macro: ;            ' t-;;         macro: ;;
' t-=if        macro: =if          ' t->if        macro: >if
' t-<if        macro: <if          ' t-!if        macro: !if
' t-then       macro: then         ' t-repeat     macro: repeat
' t-again      macro: again        ' t-0;         macro: 0;
' t-(          macro: (            ' t-push       macro: push
' t-pop        macro: pop          ' t-[']        macro: [']
' t-for        macro: for          ' t-next       macro: next

( Data )
  tx           data: tx            ty           data: ty
  last         data: last          compiler     data: compiler
  TIB          data: tib           update       data: update
  fb           data: fb            fw           data: fw
  fh           data: fh            #mem         data: #mem

include extensions

patch-init-chain
patch-dictionary

main:
  initialize listen

cross-summary
end
