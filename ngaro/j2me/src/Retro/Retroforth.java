package Retro;

/**********************************************************************
 * RETRO 10 J2ME
 * Written by Martin Polak
 * Released into the public domain.
 *
 **********************************************************************/

import javax.microedition.midlet.*;
import javax.microedition.lcdui.*;

public class Retroforth extends MIDlet {
  public static Retroforth instance;
  public static final int IMAGE_SIZE  = 50000;
  public static int [] image = new int[IMAGE_SIZE];
  Display mainDisplay;

  public Retroforth() {
    instance = this;
  }
  
  public void startApp() {
    Img im = new Img();
    mainDisplay = Display.getDisplay(this);
    mainDisplay.setCurrent(new EvalForm(mainDisplay));
  }

  public void pauseApp() {
  }

  public void destroyApp(boolean unconditional) {
  }

  public static void quitApp() {
    instance.destroyApp(true);
    instance.notifyDestroyed();
    instance = null;
  }
}
