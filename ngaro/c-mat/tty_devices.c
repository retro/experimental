/******************************************************
 * Ngaro
 *
 *|F|
 *|F| FILE: devices.c
 *|F|
 *
 * Written by Charles Childers, released into the public
 * domain
 ******************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <termios.h>

#include "functions.h"
#include "vm.h"

/* From vm.c */
extern VM_STATE vm;

struct termios new_termios, old_termios;


/******************************************************
 *|F| void draw_character(int character)
 ******************************************************/
void draw_character(int character)
{
  putchar((char)character);
}


/******************************************************
 *|F| void init_devices()
 ******************************************************/
void init_devices()
{
  tcgetattr(0, &old_termios);
  new_termios = old_termios;
  new_termios.c_iflag &= ~(BRKINT+ISTRIP+IXON+IXOFF);
  new_termios.c_iflag |= (IGNBRK+IGNPAR);
  new_termios.c_lflag &= ~(ICANON+ISIG+IEXTEN+ECHO);
  new_termios.c_cc[VMIN] = 1;
  new_termios.c_cc[VTIME] = 0;
  tcsetattr(0, TCSANOW, &new_termios);
}



/******************************************************
 *|F| void cleanup_devices()
 ******************************************************/
void cleanup_devices()
{
  tcsetattr(0, TCSANOW, &old_termios);
}
