/******************************************************
 * Ngaro
 *
 *|F|
 *|F| FILE: ngaro.c
 *|F|
 *
 * Written by Charles Childers, released into the public 
 * domain
 ******************************************************/

#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "functions.h"
#include "vm.h"

extern VM_STATE vm;


/******************************************************
 *|F| int main(int argc, char **argv)
 ******************************************************/
int main(int argc, char **argv)
{
  int a, i, trace, endian;

  trace = 0;
  endian = 0;

  if (argc < 2)
  {
    fprintf(stderr, "Ngaro Threading VM by Matthias Schirm\n\n");
    fprintf(stderr, "%s option imagename\n", argv[0]);
    fprintf(stderr, "Option can be one of the following:\n");
    fprintf(stderr, "   --endian   Load an image with a different endianness\n");
    exit(-1);
  }

  init_vm();

  for (i = 1; i < argc; i++)
  {
    if (strcmp(argv[i], "--endian") == 0)
    {
      endian = 1;
    }
    else
    {
      a = vm_load_image(argv[i]);
      strcpy(vm.filename, argv[i]);
    }
  }

  if (endian == 1)
    swapEndian();

  init_devices();
  vm_process();

  cleanup_devices();
  return 0;
}
