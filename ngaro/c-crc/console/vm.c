/******************************************************
 * Ngaro
 *
 *|F|
 *|F| FILE: vm.c
 *|F|
 *
 * Written by Charles Childers, released into the public 
 * domain
 ******************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <termios.h>

#include "functions.h"
#include "vm.h"

/* Variables specific to the VM */
VM_STATE vm;

struct termios new_termios, old_termios;



/******************************************************
 *|F| void init_vm()
 *
 * This zeros out everything in the VM_STATE struct
 * to give us a known starting point.
 ******************************************************/
void init_vm()
{
   int a;
   vm.ip = vm.sp = vm.rsp = 0;
   for (a = 0; a < STACK_DEPTH; a++)
      vm.data[a] = 0;
   for (a = 0; a < ADDRESSES; a++)
      vm.address[a] = 0;
   for (a = 0; a < IMAGE_SIZE; a++)
      vm.image[a] = 0;
   for (a = 0; a < 1024; a++)
      vm.ports[a] = 0;
}



/******************************************************
 *|F| void vm_process()
 *
 * This is a simple switch-based processor with one
 * case for each opcode. It's not the fastest approach,
 * but is easy enough to follow, add on to, and works
 * well enough for my current needs.
 ******************************************************/
void vm_process(int opcode) {
  int a, b;

  switch(opcode)
  {
    case VM_NOP:
         break;
    case VM_LIT:
         vm.sp++;
         vm.ip++;
         TOS = vm.image[vm.ip];
         break;
    case VM_DUP:
         vm.sp++;
         vm.data[vm.sp] = NOS;
         break;
    case VM_DROP:
         DROP
         break;
    case VM_SWAP:
         a = TOS;
         TOS = NOS;
         NOS = a;
         break;
    case VM_PUSH:
         vm.rsp++;
         TORS = TOS;
         DROP
         break;
    case VM_POP:
         vm.sp++;
         TOS = TORS;
         vm.rsp--;
         break;
    case VM_CALL:
         vm.ip++;
         vm.rsp++;
         TORS = vm.ip++;
         vm.ip = vm.image[vm.ip-1] - 1;
         break;
    case VM_JUMP:
         vm.ip++;
         vm.ip = vm.image[vm.ip] - 1;
         break;
    case VM_RETURN:
         vm.ip = TORS;
         vm.rsp--;
         break;
    case VM_GT_JUMP:
         vm.ip++;
         if(NOS > TOS)
           vm.ip = vm.image[vm.ip] - 1;
         DROP DROP
         break;
    case VM_LT_JUMP:
         vm.ip++;
         if(NOS < TOS)
           vm.ip = vm.image[vm.ip] - 1;
         DROP DROP
         break;
    case VM_NE_JUMP:
         vm.ip++;
         if(TOS != NOS)
           vm.ip = vm.image[vm.ip] - 1;
         DROP DROP
         break;
    case VM_EQ_JUMP:
         vm.ip++;
         if(TOS == NOS)
           vm.ip = vm.image[vm.ip] - 1;
         DROP DROP
         break;
    case VM_FETCH:
         TOS = vm.image[TOS];
         break;
    case VM_STORE:
         vm.image[TOS] = NOS;
         DROP DROP
         break;
    case VM_ADD:
         NOS += TOS;
         DROP
         break;
    case VM_SUB:
         NOS -= TOS;
         DROP
         break;
    case VM_MUL:
         NOS *= TOS;
         DROP
         break;
    case VM_DIVMOD:
         a = TOS;
         b = NOS;
         TOS = b / a;
         NOS = b % a;
         break;
    case VM_AND:
         a = TOS;
         b = NOS;
         DROP
         TOS = a & b;
         break;
    case VM_OR:
         a = TOS;
         b = NOS;
         DROP
         TOS = a | b;
         break;
    case VM_XOR:
         a = TOS;
         b = NOS;
         DROP
         TOS = a ^ b;
         break;
    case VM_SHL:
         a = TOS;
         b = NOS;
         DROP
         TOS = b << a;
         break;
    case VM_SHR:
         a = TOS;
         b = NOS;
         DROP
         TOS = b >>= a;
         break;
    case VM_ZERO_EXIT:
         if (TOS == 0)
         {
           DROP
           vm.ip = TORS;
           vm.rsp--;
         }
         break;
    case VM_INC:
         TOS += 1;
         break;
    case VM_DEC:
         TOS -= 1;
         break;
    case VM_IN:
         a = TOS;
         TOS = vm.ports[a];
         vm.ports[a] = 0;
         break;
    case VM_OUT:
         vm.ports[0] = 0;
         vm.ports[TOS] = NOS;
         DROP DROP
         break;
    case VM_WAIT:
         if (vm.ports[0] == 1)
           break;

         /* Input */
         if (vm.ports[0] == 0 && vm.ports[1] == 1)
         {
           vm.ports[1] = getchar();
           vm.ports[0] = 1;
         }

         /* Output (character generator) */
         if (vm.ports[2] == 1)
         {
           putchar((char)TOS); DROP
           vm.ports[2] = 0;
           vm.ports[0] = 1;
         }

         /* Save Image */
         if (vm.ports[4] == 1)
         {
           vm_save_image(vm.filename);
           vm.ports[4] = 0;
           vm.ports[0] = 1;
         }

         /* Capabilities */
         if (vm.ports[5] == -1)
         {
           vm.ports[5] = IMAGE_SIZE;
           vm.ports[0] = 1;
         }

         /* The framebuffer related bits aren't supported, so return 0 for them. */
         if (vm.ports[5] == -2 || vm.ports[5] == -3 || vm.ports[5] == -4)
         {
           vm.ports[5] = 0;
           vm.ports[0] = 1;
         }
         break;
    default:
         vm.ip = IMAGE_SIZE;
         break; 
  }
}



/******************************************************
 *|F| void init_devices()
 ******************************************************/
void init_devices()
{
  tcgetattr(0, &old_termios);
  new_termios = old_termios;
  new_termios.c_iflag &= ~(BRKINT+ISTRIP+IXON+IXOFF);
  new_termios.c_iflag |= (IGNBRK+IGNPAR);
  new_termios.c_lflag &= ~(ICANON+ISIG+IEXTEN+ECHO);
  new_termios.c_cc[VMIN] = 1;
  new_termios.c_cc[VTIME] = 0;
  tcsetattr(0, TCSANOW, &new_termios);
}



/******************************************************
 *|F| void cleanup_devices()
 ******************************************************/
void cleanup_devices()
{
  tcsetattr(0, TCSANOW, &old_termios);
}
