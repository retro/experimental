/******************************************************
 * Ngaro
 *
 *|F|
 *|F| FILE: loader.c
 *|F|
 *
 * Written by Charles Childers, released into the public 
 * domain
 ******************************************************/

#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>

#include "functions.h"
#include "vm.h"

extern VM_STATE vm;



/******************************************************
 *|F| int vm_load_image(char *image)
 * Description:
 *   This is used to load an image to the vm.image[]
 *   buffer
 ******************************************************/
int vm_load_image(char *image)
{
  FILE *fp;
  int x;

  if ((fp = fopen(image, "rb")) == NULL)
  {
    fprintf(stderr, "Sorry, but I couldn't open %s\n", image);
    exit(-1);
  }

  x = fread(&vm.image, sizeof(int), IMAGE_SIZE, fp);
  fclose(fp);

  return x;
}


/******************************************************
 *|F| int vm_save_image(char *image)
 * Description:
 *   This is used to save an image from the vm.image[]
 *   buffer
 ******************************************************/
int vm_save_image(char *image)
{
  FILE *fp;
  int x;

  if ((fp = fopen(image, "w")) == NULL)
  {
    fprintf(stderr, "Sorry, but I couldn't open %s\n", image);
    exit(-1);
  }

  x = fwrite(&vm.image, sizeof(int), IMAGE_SIZE, fp);
  fclose(fp);

  return x;
}
