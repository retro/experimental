
/******************************************************
 * Ngaro
 *
 *|F|
 *|F| FILE: ngaro_windows.c
 *|F|
 *
 * Written by Charles Childers, released into the public
 * domain
 *
 * Ngaro wasn't originally designed to be run on Windows.
 * This is a custom main() and support code to support
 * it in a crude way. It's not ideal, but is sufficient
 * for now.
 *
 * One day we may get someone who knows Win32 and/or
 * .NET well enough to bring a nice Ngaro VM to the
 * Windows world.
 ******************************************************/

#include <windows.h>

#include "functions.h"
#include "vm.h"

extern VM_STATE vm;

/*  Declare Windows procedure  */
LRESULT CALLBACK WindowProcedure (HWND, UINT, WPARAM, LPARAM);
char szClassName[ ] = "WindowsApp";



/******************************************************
 *|F| int ngaro_process_image(void *unused)
 ******************************************************/
int ngaro_process_image(void *unused)
{
  for (vm.ip = 0; vm.ip < IMAGE_SIZE; vm.ip++)
    vm_process(vm.image[vm.ip]);

  cleanup_devices();
  exit(0);
}



/******************************************************
 *|F| int WinMain(HINSTANCE hThisInstance,
 *|F|             HINSTANCE hPrevInstance,
 *|F|             LPSTR lpszArgument,
 *|F|             int nFunsterStil)
 ******************************************************/
int WINAPI WinMain (HINSTANCE hThisInstance,
                    HINSTANCE hPrevInstance,
                    LPSTR lpszArgument,
                    int nFunsterStil)
{
  HWND hwnd;               /* This is the handle for our window */
  MSG messages;            /* Here messages to the application are saved */
  WNDCLASSEX wincl;        /* Data structure for the windowclass */

  /* The Window structure */
  wincl.hInstance = hThisInstance;
  wincl.lpszClassName = szClassName;
  wincl.lpfnWndProc = WindowProcedure;      /* This function is called by windows */
  wincl.style = CS_DBLCLKS;                 /* Catch double-clicks */
  wincl.cbSize = sizeof (WNDCLASSEX);

  /* Use default icon and mouse-pointer */
  wincl.hIcon = LoadIcon (NULL, IDI_APPLICATION);
  wincl.hIconSm = LoadIcon (NULL, IDI_APPLICATION);
  wincl.hCursor = LoadCursor (NULL, IDC_ARROW);
  wincl.lpszMenuName = NULL;                 /* No menu */
  wincl.cbClsExtra = 0;                      /* No extra bytes after the window class */
  wincl.cbWndExtra = 0;                      /* structure or the window instance */

  /* Use Windows's default color as the background of the window */
  wincl.hbrBackground = (HBRUSH) COLOR_BACKGROUND;

  /* Register the window class, and if it fails quit the program */
  if (!RegisterClassEx (&wincl))
    return 0;

  /* The class is registered, let's create the program*/
  hwnd = CreateWindowEx (
         0,                   /* Extended possibilites for variation */
         szClassName,         /* Classname */
         "Windows App",       /* Title Text */
         WS_OVERLAPPEDWINDOW, /* default window */
         CW_USEDEFAULT,       /* Windows decides the position */
         CW_USEDEFAULT,       /* where the window ends up on the screen */
         544,                 /* The programs width */
         375,                 /* and height in pixels */
         HWND_DESKTOP,        /* The window is a child-window to desktop */
         NULL,                /* No menu */
         hThisInstance,       /* Program Instance handler */
         NULL                 /* No Window Creation data */
         );

  /* Make the window visible on the screen */
  /*  ShowWindow (hwnd, nFunsterStil); */

  init_vm();
  init_devices();

  vm_load_image("forth.image");
  vm.filename = (char *)"forth.image";
  SDL_CreateThread(&ngaro_process_image, NULL);

  /* Run the message loop. It will run until GetMessage() returns 0 */
  while (GetMessage(&messages, NULL, 0, 0) || vm.ip != IMAGE_SIZE)
  {
    /* Translate virtual-key messages into character messages */
    TranslateMessage(&messages);
    /* Send message to WindowProcedure */
    DispatchMessage(&messages);
  }

  cleanup_devices();

  /* The program return-value is 0 - The value that PostQuitMessage() gave */
  return messages.wParam;
}


/*  This function is called by the Windows function DispatchMessage()  */
LRESULT CALLBACK WindowProcedure (HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
  switch (message)                /* handle the messages */
  {
    case WM_DESTROY:
      cleanup_devices();
      PostQuitMessage (0);        /* send a WM_QUIT to the message queue */
      break;
    default:                      /* for messages that we don't deal with */
      return DefWindowProc (hwnd, message, wParam, lParam);
  }
  return 0;
}
