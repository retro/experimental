#!/usr/bin/perl 
#===============================================================================
#
#         FILE:  ngaro.pl
#
#        USAGE:  ./ngaro.pl 
#
#  DESCRIPTION:  Ngaro VM implementation in Perl
#
#      OPTIONS:  ---
# REQUIREMENTS:  ---
#         BUGS:  ---
#        NOTES:  ---
#       AUTHOR:  Charles Childers (), <>
#      COMPANY:  
#      VERSION:  1.0
#      CREATED:  06/01/2008 10:52:37 AM EDT
#     REVISION:  ---
#===============================================================================

use strict;
use warnings;

use Switch;
use File::stat;

package ngaro;
use enum qw(VM_NOP VM_LIT VM_DUP VM_DROP VM_SWAP VM_PUSH VM_POP VM_CALL VM_JUMP VM_RETURN VM_GT_JUMP VM_LT_JUMP VM_NE_JUMP VM_EQ_JUMP VM_FETCH VM_STORE VM_ADD VM_SUB VM_MUL VM_DIVMOD VM_AND VM_OR VM_XOR VM_SHL VM_SHR VM_ZERO_EXIT VM_INC VM_DEC VM_IN VM_OUT VM_WAIT);

my $data_file = "forth.image";
my $ip = 0;
my $sp = 0;
my $rsp = 0;
my $xx = 0;
my $xy = 0;
my @image = 0;
my @data = 0;
my @address = 0;
my @ports = 0;

open FILE, $data_file || die "Could not open forth.image!";
binmode FILE;

my $x = 0;
while ((my $n = read FILE, my $data, 4) != 0)
{
  $image[$x] = unpack 'l', $data;
  $x++;
}
close FILE;

$ports[0] = 0;
$ports[1] = 0;
$ports[2] = 0;

for ($xx = 0; $xx < 1024; $xx++)
{
  $data[$xx] = 0;
  $address[$xx] = 0;
}

for ($ip = 0; $ip < 5000000; $ip++)
{
  my $val = $image[$ip];
  $xx = 0; $xy = 0;

  switch ($val)
  {
    case VM_NOP       { }
    case VM_LIT       { $sp++; $ip++; $data[$sp] = $image[$ip]; }
    case VM_DUP       { $sp++; $data[$sp] = $data[$sp-1]; }
    case VM_DROP      { $sp--; }
    case VM_SWAP      { $xx = $data[$sp]; $xy = $data[$sp-1]; $data[$sp] = $xy; $data[$sp-1] = $xx; }
    case VM_PUSH      { $rsp++; $address[$rsp] = $data[$sp]; $sp--; }
    case VM_POP       { $sp++; $data[$sp] = $address[$rsp]; $rsp--; }
    case VM_CALL      { $ip++; $rsp++; $address[$rsp] = $ip++; $xx = $image[$ip - 1]; $ip = $xx - 1; }
    case VM_JUMP      { $ip++; $xx = $image[$ip]; $ip = $xx - 1; }
    case VM_RETURN    { $ip = $address[$rsp]; $rsp--; }
    case VM_GT_JUMP   { $ip++; $xx = $data[$sp--]; $xy = $data[$sp--]; print "ip: " . $ip . " sp: " . $sp . " xx:" . $xx . " xy:" . $xy . "\n"; if ($xy >  $xx) { $xx = $image[$ip]; $ip = $xx - 1; } }
    case VM_LT_JUMP   { $ip++; $xx = $data[$sp--]; $xy = $data[$sp--]; print "ip: " . $ip . " sp: " . $sp . " xx:" . $xx . " xy:" . $xy . "\n"; if ($xy <  $xx) { $xx = $image[$ip]; $ip = $xx - 1; } }
    case VM_NE_JUMP   { $ip++; $xx = $data[$sp--]; $xy = $data[$sp--]; print "ip: " . $ip . " sp: " . $sp . " xx:" . $xx . " xy:" . $xy . "\n"; if ($xx != $xy) { $xx = $image[$ip]; $ip = $xx - 1; } }
    case VM_EQ_JUMP   { $ip++; $xx = $data[$sp--]; $xy = $data[$sp--]; print "ip: " . $ip . " sp: " . $sp . " xx:" . $xx . " xy:" . $xy . "\n"; if ($xx == $xy) { $xx = $image[$ip]; $ip = $xx - 1; } }
    case VM_FETCH     { $xx = $data[$sp]; $data[$sp] = $image[$xx]; }
    case VM_STORE     { $xx = $data[$sp-1]; $image[$data[$sp]] = $xx; $sp--; $sp--; }
    case VM_ADD       { $data[$sp-1] = $data[$sp-1] + $data[$sp]; $sp--; }
    case VM_SUB       { $data[$sp-1] = $data[$sp-1] - $data[$sp]; $sp--; }
    case VM_MUL       { $data[$sp-1] = $data[$sp-1] * $data[$sp]; $sp--; }
    case VM_DIVMOD    { print "DIVMOD Not Implemented Yet\n"; }
    case VM_AND       { $xx = $data[$sp-1]; $xy = $data[$sp]; $sp--; $data[$sp] = $xx & $xy; }
    case VM_OR        { $xx = $data[$sp-1]; $xy = $data[$sp]; $sp--; $data[$sp] = $xx | $xy; }
    case VM_XOR       { $xx = $data[$sp-1]; $xy = $data[$sp]; $sp--; $data[$sp] = $xx ^ $xy; }
    case VM_SHL       { $xx = $data[$sp-1]; $xy = $data[$sp]; $sp--; $data[$sp] = $xx << $xy; }
    case VM_SHR       { $xx = $data[$sp-1]; $xy = $data[$sp]; $sp--; $data[$sp] = $xx >> $xy; }
    case VM_ZERO_EXIT { $xx = $data[$sp]; $xy = $address[$rsp]; if ($xx == 0) { $sp--; $ip = $xy; $rsp--; } }
    case VM_INC       { $data[$sp]++; }
    case VM_DEC       { $data[$sp]--; }
    case VM_IN        { $xx = $data[$sp]; $data[$sp] = $ports[$xx]; }
    case VM_OUT       { $xx = $data[$sp]; $sp--; $xy = $data[$sp]; $sp--; $ports[$xx] = $xy; }
    case VM_WAIT      { if ($ports[0] != 1)
                        {
                          if ($ports[0] == 0 && $ports[1] == 1)
                          {
                            $ports[1] = unpack 'l', getc STDIN;
                            $ports[0] = 1;
                          }
                          if ($ports[2] == 1)
                          {
                            $xx = pack 'c', $data[$sp];
                            print $xx;
                            $sp--;
                            $ports[2] = 0;
                            $ports[0] = 1;
                          }
                        }
                      }
    else              { print "Unrecognized Opcode! " . $val . "\n"; exit 0; }
  }
}
