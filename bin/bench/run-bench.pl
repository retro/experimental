#!/usr/bin/perl
#===============================================================================
#
#         FILE:  run-bench.pl
#
#        USAGE:  ./run-bench.pl
#
#  DESCRIPTION:  Benchmarks for Retro
#
#      OPTIONS:  ---
# REQUIREMENTS:  ---
#         BUGS:  ---
#        NOTES:  ---
#       AUTHOR:  Charles Childers (), <>
#      COMPANY:
#      VERSION:  1.0
#      CREATED:  06/01/2008 10:52:37 AM EDT
#     REVISION:  ---
#===============================================================================

use strict;
use warnings;

package retrobench;
use Time::HiRes qw( gettimeofday tv_interval );
use File::Copy;

my ($t1, $t2);


print "Benchmarks for Retro\n";
print "----------------------------------------\n";

print "Preparing image... ";
  copy "../forth.image", "benchmark.image" or die "Failed to copy original image!\n";
  system "cat words.retro | ../ngaro benchmark.image >/dev/null";
  print "ok\n";

print "Running Benchmarks:\n";
  print " - Recursive FIB (39)... ";
    $t1 = [gettimeofday];
    system "cat tests/fib.test | ../ngaro benchmark.image >/dev/null";
    $t2 = (tv_interval($t1) * 1e6 / 1000) / 1000;
    print "$t2 seconds\n"; 

  print " - Countdown Loop (1,000,000)... ";
    $t1 = [gettimeofday];
    system "cat tests/loop.test | ../ngaro benchmark.image >/dev/null";
    $t2 = (tv_interval($t1) * 1e6 / 1000) / 1000;
    print "$t2 seconds\n"; 

  print " - Nest/Unnest (256 million pairs)... ";
    $t1 = [gettimeofday];
    system "cat tests/nest.test | ../ngaro benchmark.image >/dev/null";
    $t2 = (tv_interval($t1) * 1e6 / 1000) / 1000;
    print "$t2 seconds\n"; 

print "Removing image... ";
  unlink "benchmark.image" or die "Unable to remove image!\n";
  print "ok\n";
